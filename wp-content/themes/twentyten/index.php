<?php
/**
 * Main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
$page_id = get_the_ID();
$theme_dir = get_template_directory_uri();
$page_data = get_page( $page_id );
$body_id = strtolower(str_replace(" ", "-", $page_data->post_title));

get_header(); ?>
<link href="<?php echo $theme_dir; ?>/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo $theme_dir; ?>/css/bootstrap-theme.min.css" rel="stylesheet">
<link href="<?php echo $theme_dir; ?>/css/main.css" rel="stylesheet">

<script src="<?php echo $theme_dir; ?>/js/jquery-1.11.2.min.js"></script>
<script src="<?php echo $theme_dir; ?>/js/functions.js"></script>

		<div id="container">
			<div id="content" role="main">

			</div><!-- #content -->
		</div><!-- #container -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
