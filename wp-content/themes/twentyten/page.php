<?php
/**
 * Template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
$page_id = get_the_ID();
$theme_dir = get_template_directory_uri();
$page_data = get_page( $page_id );
$body_id = strtolower(str_replace(" ", "-", $page_data->post_title));
get_header(); ?>

<link href="<?php echo $theme_dir; ?>/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo $theme_dir; ?>/css/bootstrap-theme.min.css" rel="stylesheet">
<link href="<?php echo $theme_dir; ?>/css/main.css" rel="stylesheet">

<script src="<?php echo $theme_dir; ?>/js/jquery-1.11.2.min.js"></script>
<script src="<?php echo $theme_dir; ?>/js/functions.js"></script>

		<div id="container">
			<div id="content" role="main">
			<?php
				switch ($page_id) {
					case 4:
						// Example 1

						break;
					case 6:
						// Example 2

						break;
					default:
						echo $page_data->post_content;

				}
				?>
			<?php
			/*
			 * Run the loop to output the page.
			 * If you want to overload this in a child theme then include a file
			 * called loop-page.php and that will be used instead.
			 */
			//get_template_part( 'loop', 'page' );
			?>

			</div><!-- #content -->
		</div><!-- #container -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
