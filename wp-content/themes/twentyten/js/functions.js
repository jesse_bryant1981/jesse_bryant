var rss_base = 'http://feeds.bbci.co.uk/news/';
var rss_feeds = [
	{
		name: 'Top Stories',
		url: rss_base+'rss.xml'
	},
	{
		name: 'Business',
		url: rss_base+'business/rss.xml'
	},
	{
		name: 'Technology',
		url: rss_base+'technology/rss.xml'
	}
]
$(document).ready(function(){
	initRSS();
});
function initRSS() {
	var container = $('#container');
	var obj;
	if (container.length) {
		for (var i in rss_feeds) {
			container.append('<div class="rss-wrapper"></div>');
			obj = container.find('.rss-wrapper:last');
			fetchRSS(rss_feeds[i].url,obj,rss_feeds[i].name)
		}
	}
}
function fetchRSS(url,obj,name) {
	$.ajax({
		url: '/getRss.php',
		data: 'url='+url,
		type: 'POST',
		datatype: 'xml',
		success: function(res) {
			renderRSS(res,obj,name);
		}
	});
}
function renderRSS(res,obj,name) {
	var title;
	var url;
	var caption;
	var pubdate;
	var count = 0;
	
	obj.append('<h2>'+name+'</h2>');
	$(res).find('channel item').each(function() {
		if (count == 20) return;
		title	= $(this).find('title').text();
		url		= $(this).find('link')[0].nextSibling.data;
		caption = $(this).find('description').text();
		pubdate = $(this).find('pubDate').text();
		
		caption = caption.substr(0,128);
		
		obj.append('<div class="rss-item bg-info"><a href="'+url+'" target="_new">'+title+'</a><br/><span class="rss-caption">'+caption+'</span><br/><span class="rss-published">'+pubdate+'</span></div>');
		count++;
	});
}