<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'test');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'qX=~+M-dY8>-q938_.{k>k,>`?n$gK8aEfI>B,iA|X%-5Hr?J;TzdQXeg+PK0<K)');
define('SECURE_AUTH_KEY',  ',<~r~x8[ZN7N;Df;#CcCv#$:Ow4q1D;G2!Tt*v<<#$IHd7.|%ex3]a6O2#^wslqi');
define('LOGGED_IN_KEY',    'D:|L )6W.<RR;%3)1~Y~FB|+Mds+1:+iFnj%B2; C^.`+^Ow@m_TG$2[AO8M!rm=');
define('NONCE_KEY',        'hL_we^F>3iEMRzv$/X0YdH~& 8gSEe9719Na}F}YH]SYyS%7JO5v},2 KM9uu|H}');
define('AUTH_SALT',        '&/[G:E?-qQXj7q%`}R#l+7`SMgjCPpEy]@tvM[oct%<CUS$wAmkV/*6+{{W*)97^');
define('SECURE_AUTH_SALT', '.g$e|8|rQJ}9Z<zBg1x=9q(aINjq1S_ei_UW_wT,8]_w*:k@?vkZn!`DV6]FZMy<');
define('LOGGED_IN_SALT',   '1Yu-bTY4{@V$|(|25+a9rEjzHiVD2$e;Ads1]*A%J5/Ar=ojM|rwZqo;TQ/R%8#Y');
define('NONCE_SALT',       ',ZJ7$xhq ]y9WVL8+Ha;71vdGXeRPWNN|esb/>vM1-v[U: <&,ds9;d7EIl.k+wS');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
